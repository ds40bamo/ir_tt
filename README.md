# Ted Search
This project was created in the context of the [information retrieval lecture](https://temir.org/teaching/information-retrieval.html) given by [Jun.-Prof. Dr. Martin Potthast](https://www.linkedin.com/in/potthast/) during the summer semester 2019 at the [University of Leipzig](https://www.uni-leipzig.de/).
This project is a fork of the original [Ted Search project](https://git.informatik.uni-leipzig.de/bg40lecu/ir_tt) and replaces the elasticserach server with a LSI approach for evaluation of the original project.

# Initialization

**requirements**: install `docker` and `docker-compose` on your system and make sure the docker service is running
    
**note**: all paths are relative to the project-root

1. open a terminal and cd into the `/docker/` folder
2. run `docker-compose up` to start the application (use the `-d` flag, if you want to close the console without terminating the application)
    (**note**: run `docker-compose down` inside `/docker/` to terminate the application)
    the following containers should be running:

    * docker\_ir\_1

3. The searchengine can now be accessed via `localhost:5000`

# Usage
The normal search modus can be accessed via `localhost:5000`.
The evaluation modus can be accessed via `localhost:5000/eval`.

Use the `/bin/concon` script to enter the container for additional functionality:

* use `flask evaluate` to show the current effectiveness of the searchengine

# Dataset
The used dataset was created by [Rounak Banik](https://www.kaggle.com/rounakbanik) and can be found [here](https://www.kaggle.com/rounakbanik/ted-talks).
Note that the dataset was published under the [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) license.
Therefore, our modified version [tedtalks.csv](https://git.informatik.uni-leipzig.de/ds40bamo/ir_tt/blob/master/searchengine/csv/tedtalks.csv) is also subject to this license.

# License
Our project is subject to the [GNU LGPLv3](https://git.informatik.uni-leipzig.de/ds40bamo/ir_tt/blob/master/LICENSE) license.

