from flask import render_template
from flask import jsonify
from flask import request
from app import app, frontend
from app.forms import QueryForm, TopicQueryForm
from app.evaluation import Topic, load_all_topics

lsiindex = app.index

@app.route('/', methods=['GET'])
def index():
    form = QueryForm()
    if form.validate():
        response = lsiindex.query(form.query.data)
        hit_list = frontend.response_list_constructor(response)
        return render_template('searchresults.html', form=form, hits=hit_list)
    else:
        return render_template('searchindex.html', form=form)


@app.route('/eval', methods=['GET', 'POST'])
def evaluate():
    form = TopicQueryForm()

    topic_dict = load_all_topics()
    topics = [(topic.id, topic.query) for _, topic in topic_dict.items()]
    topics.sort()

    if form.validate_on_submit():
        id = form.topic_id.data
        topic = Topic.load_by_id(id)
        relevances = topic.relevances
        hits = lsiindex.query(topic.query)
        hit_list = frontend.response_list_constructor(hits)
        for hit in hit_list:
            hit.tid = str(hit.tid)
        return render_template('eval.html', form=form, topics=topics, topic=topic, hits=hit_list, relevances=relevances)

    else:
        return render_template('evalindex.html', form=form, topics=topics)


@app.route('/topiceval', methods=['POST'])
def topiceval():
    try:
        topic_id = request.form['topic_id']
        document_id = request.form['document_id']
        relevance = request.form['relevance']

        evaluated_topic = Topic.load_by_id(topic_id)
        evaluated_topic.add_relevances({document_id: relevance})
        evaluated_topic.save()
    except Exception as e:
        return jsonify({"success": False})

    return jsonify({"success": True})

