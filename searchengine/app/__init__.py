from flask import Flask
from config import Config


app = Flask(__name__)
app.config.from_object(Config)

from app.indexing import Index
app.index = Index.load()

from app import routes, indexing, evaluation, init_topics
