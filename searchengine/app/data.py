from ast import literal_eval
from os import path
import csv

PATH = path.join(path.dirname(path.abspath(__file__)), "..", "csv")
CSVPATHRAW = path.join(PATH, "tedtalks.csv")
CSVPATHLEMMA= path.join(PATH, "tedlemmatized.csv")

def load_all_instances():
    with open(CSVPATHRAW) as csvfileraw:
        readerraw = csv.DictReader(csvfileraw)
        with open(CSVPATHLEMMA) as csvfilelemma:
            readerlemma = csv.DictReader(csvfilelemma)

            talks = []
            try:
                while True:
                    talk = next(readerraw)
                    talklemma = next(readerlemma)
                    talk["ratings"] = literal_eval(talk["ratings"])
                    talk["related_talks"] = literal_eval(talk["related_talks"])
                    talk["lemma"] = talklemma["transcript"]
                    talks.append(talk)
            except:
                pass

    return talks
