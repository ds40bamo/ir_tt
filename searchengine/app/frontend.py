from app import app
class FrontendObject:
    # TODO: Add possibly useful metadata information
    def __init__(self, tid, title, score, ranking,
                 raw_script, snippet, embedding, thumbnail_url):
        self.tid = tid
        self.title = title
        self.score = score
        self.ranking = ranking
        self.raw_script = raw_script
        self.snippet = snippet
        self.embedding = embedding
        self.thumbnail_url = thumbnail_url


def embedding_generator(url):
    parts = url.split(".")
    parts[0] = 'https://embed'
    return ".".join(parts)


def response_list_constructor(response):
    response_list = []
    ranking_counter = 0
    for hit, score in response:
        ranking_counter += 1
        response_object = FrontendObject(
            tid=hit["id"],
            title=hit["title"],
            score=score,
            ranking=ranking_counter,
            raw_script=hit["transcript"],
            snippet=hit["description"],
            embedding= embedding_generator(hit["url"]),
            thumbnail_url=hit["thumbnail_url"],
        )

        response_list.append(response_object)

    return response_list
