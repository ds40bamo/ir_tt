import nltk
import click
from os import path
from gensim import corpora
from gensim import models
from gensim import similarities
from app.data import load_all_instances
from app import app
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet


DATAPATH = "/tedtalkdata"
DICTPATH = path.join(DATAPATH, "teddict.dict")
CORPUSPATH = path.join(DATAPATH, "tedcorpus.mm")
TFIDFPATH = path.join(DATAPATH, "model.tfidf")
LSIPATH = path.join(DATAPATH, "model.lsi")
INDEXPATH = path.join(DATAPATH, "ted.index")


class Index():
    lemmatize = False
    stem = True
    stop = True
    num_topics = 100
    stemmer = nltk.stem.SnowballStemmer("english")
    def __init__(self, data, dictionary, corpus, tfidf, lsi, index):
        self.data = data
        self.dictionary = dictionary
        self.corpus = corpus
        self.tfidf = tfidf
        self.lsi = lsi
        self.index = index


    def query(self, text, start=0, size=10):
        query = nltk.word_tokenize(text.lower())
        if Index.lemmatize:
            tag_dict = {"J": wordnet.ADJ,
                        "N": wordnet.NOUN,
                        "V": wordnet.VERB,
                        "R": wordnet.ADV}
            lemmatizer = WordNetLemmatizer()
            query = [lemmatizer.lemmatize(token, tag_dict.get(tag[0], wordnet.NOUN)) for token, tag in nltk.pos_tag(query)]

        if Index.stem:
            query = list(map(lambda x: Index.stemmer.stem(x), query))
        query_vec = self.dictionary.doc2bow(query)
        query_vec_lsi = self.lsi[self.tfidf[query_vec]]

        sims = self.index[query_vec_lsi]
        sims = sorted(list(enumerate(sims)), key=lambda x: x[1], reverse=True)
        results = sims[start:start+size]
        results = [(self.data[id], score) for id, score in results]

        return results


    @staticmethod
    def create():
        app.logger.info("load")
        data = load_all_instances()
        if Index.lemmatize:
            textcorpus = [obj["lemma"].lower().split() for obj in data]
        else:
            if Index.stem:
                textcorpus = [obj["transcript_stem"].lower().split("|") for obj in data]
            else:
                textcorpus = [nltk.word_tokenize(obj["transcript"].lower()) for obj in data]

        if Index.stem and Index.lemmatize:
            app.logger.info("stemming")
            textcorpus = [list(map(lambda x: Index.stemmer.stem(x), doc)) for doc in textcorpus]
        dictionary = corpora.Dictionary(textcorpus)

        app.logger.info("stopping")
        stopwords = nltk.corpus.stopwords.words("english")
        if Index.stop:
            app.logger.info("advanced stopping")
            stop_ids = [dictionary.token2id[stopword] for stopword in stopwords if stopword in dictionary.token2id]
            ids = stop_ids

            dictionary.filter_tokens(ids)
            dictionary.filter_n_most_frequent(30)
        corpus = [dictionary.doc2bow(document) for document in textcorpus]

        app.logger.info("tfidf")
        tfidf = models.TfidfModel(corpus)
        corpus_tfidf = tfidf[corpus]
        app.logger.info("lsi")
        lsi = models.LsiModel(corpus_tfidf, id2word=dictionary, num_topics=Index.num_topics)
        corpus_lsi = lsi[corpus_tfidf]

        app.logger.info("index")
        index = similarities.MatrixSimilarity(corpus_lsi)

        app.logger.info("save")
        dictionary.save(DICTPATH)
        corpora.MmCorpus.serialize(CORPUSPATH, corpus)
        tfidf.save(TFIDFPATH)
        lsi.save(LSIPATH)
        index.save(INDEXPATH)


    @staticmethod
    def load():
        app.logger.info("loading")
        try:
            data = load_all_instances()
            dictionary = corpora.Dictionary.load(DICTPATH)
            corpus = corpora.MmCorpus(CORPUSPATH)
            tfidf = models.TfidfModel.load(TFIDFPATH)
            lsi = models.LsiModel.load(LSIPATH)
            index = similarities.MatrixSimilarity.load(INDEXPATH)
        except:
            return None
        app.logger.info("loaded")

        return Index(data, dictionary, corpus, tfidf, lsi, index)


    @staticmethod
    def exists():
        return path.exists(DICTPATH) and \
               path.exists(CORPUSPATH) and \
               path.exists(TFIDFPATH) and \
               path.exists(LSIPATH) and \
               path.exists(INDEXPATH)


@app.cli.command()
@click.option("--force", is_flag=True)
def create_index_if_not_exists(force):
    if force or not Index.exists():
        Index.create()
