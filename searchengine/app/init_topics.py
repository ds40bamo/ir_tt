from app import app
from app.evaluation import Topic

@app.cli.command(help="creates the topic files (overwrites existing)")
def init_topics():
    topics = [
        (1, "NASA MOON", "Moonlanding, technology, NASA program, Apollo 13, Neil Armstrong, preparations"),
        (2, "anxiety and how it affects life", "Types of anxiety, life circumstances, talk by a social worker; psychologist o.ä"),
        (3, "rhetoric of politicians", "politicians controversies,language"),
        (4, "Trangenderism in mainstream", "Transgenderism as trend, Attention to issues they face, gender transistion, assault, activists"),
        (5, "doctor about ebola", "medical aspect, fear in society, stigma, speakeroccupation from medical field"),
        (6, "(Micro)plastic in our eco systems", "farmers, pollution, plastic islands in the sea"),
        (7, "WILL ROBOTS END HUMANITY", "conspiracy theories, outlooks to future of technology, current state of research, ethics"),
        (8, "universal healthcare", "obama, obamacare, reforms, social"),
        (9, "Why are whistleblowers important to democracy", "chelsea manning, edward snowden, leaked information, wiki leaks, social impact, scandal, iraq"),
        (10, "fulfilling life", "advice, plan, organizing life, happy life"),
        (11, "cancer treatment", "current state, effects of cancer, side effects of treatment, medication"),
        (12, "gen modification","How to modify DNA, crispr, technologies"),
        (13, "artificially generated art","computer generated art, artist, computer science"),
        (14, "How to be funny","comedian, techniques, charisma"),
        (15, "living in a religious cult","religion, separated, micro society, growing up"),
        (16, "motivate to buy","business,shopping, consumption"),
        (17, "Improve learning","techniques, learning, improving skills"),
        (18, "HIV treatment","infection, medical progress"),
        (19, "books","information, facts about books, history of books, how certain books shaped people's view"),
        (20, "inequality rich poor","income, social status, living conditions, career chances"),
        (21, "freedom of press in the internet","journalism, outlet for information, news"),
        (22, "Internet influence on everyday life", "social media, advertising, messaging, danger "),
        (23, "ancient architecture","greek, romans, columns, gothic, renaissance, temple"),
        (24, "internet slang","language, abbreviations, texting, social media"),
        (25, "language education","learning of languages, teaching, tricks, techniques"),
        (26, "physical assault","violence, guns, sexual assault, physical assault, consequences, effects, victim"),
        (27, "animal cruelty", "animal abuse, pet, cattle, activism, industrial livestock farming"),
        (28, "success in career","improve, work efficiently, social, €€€, leadership"),
        (29, "math in nature","romanesco, fractal, golden cut, sunflower, fibonacci"),
        (30, "what is consciousness","philosophy, reasoning, artificial Intelligence"),
        (31, "traveling ideas","limited resources, visiting many places, world, inspiration"),
        (32, "benefits of drugs","weed, marijuana, PTSD, reduce anxiety"), #check for bias
        (33, "gender inequality","social, career, gender roles"),
        (34, "positive effect of sport","sport, lifestyle, health, physical strength, stamina"),
        (35, "history of beauty","beauty standard, mindset"),
        (36, "Do vaccines cause autism", "information about whether there is a correlation between vaccines and autism"),
        (37, "string theory", "what is it, implications, problems with it"),
        (38, "google private data", "how google handles private data, are there safety concerns?"),
        (39, "basic guide economics","explanation of economy, different systems, how to work with it (individual benefit)"),
        (40, "bitcoin", "information about crypto currency especially bitcoin, how does it work, why does it work"),
        (41, "elon musk tesla", "who is elon musk, what is tesla, relation, news"),
        (42, "obesity", "effects om health, statistics, how to deal with it"),
        (43, "machine learning", "how does it work, fields where its used, benefits and potential harms"),
        (44, "do good and evil exist?", "philosophy"),
        (45, "Marianas Trench", "basic information, creatures living there, is it impossible to go there?"),
        (46, "why do we feel pain", "explanation about how pain emerges , benefits for individuals"),
        (47, "differences friendship relationship", "where are the differences? what is love? what defines friendship?"),
        (48, "personality traits", "information about personality traits, NEOAC or different models, development"),
        (49, "difference suicide rate gender", "how do genders differ in suicide rate,statistics, conclusion"),
        (50, "AI threat or opportunity", "advantages, disadvantages, risks, benefits, reasoning")
    ]

    for id, title, description in topics:
        Topic(id, title, description).save()
