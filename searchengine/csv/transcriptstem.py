#!/bin/python

import csv
import nltk


if __name__ == "__main__":
    print("reading")
    with open("tedtalks.csv", "r") as csvfile:
        reader = csv.DictReader(csvfile)
        objects = list(reader)
        stemmer = nltk.stem.SnowballStemmer("english")
        print("stemming")
        for obj in objects:
            text = nltk.word_tokenize(obj["transcript"].lower())
            text_stemmed = list(map(lambda x: stemmer.stem(x), text))
            obj["transcript_stem"] = "|".join(text_stemmed)

    print("writing")
    with open("tedtalks.csv", "w") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=objects[0].keys())
        writer.writeheader()
        writer.writerows(objects)
