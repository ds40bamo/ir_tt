#!/bin/python

import nltk
import csv
from nltk.corpus import wordnet
from nltk.stem import WordNetLemmatizer
from re import compile
from sys import stdout


tag_dict = {"J": wordnet.ADJ,
            "N": wordnet.NOUN,
            "V": wordnet.VERB,
            "R": wordnet.ADV}

expr = compile(r"([\].?!)])([\[(A-Z])")
repl = r"\1 \2"
wordpunctuation = compile("[_.-]*([a-zA-Z0-9]+)[_.-]*")
number = compile("([0-9]+(\.[0-9]+)?)")

if __name__ == "__main__":
    results = []
    with open("tedtalks.csv", "r") as csvfile:
        reader = csv.DictReader(csvfile)

        for i, obj in enumerate(reader, start=1):
            print(i, end=" ")
            stdout.flush()
            text = obj["transcript"]
            id = obj["id"]

            newtext = expr.sub(repl, text)

            tmptokens = [token for token in nltk.word_tokenize(newtext)]
            tokens = []
            for token in tmptokens:
                found = number.findall(token)
                if found:
                    tokens.append(found[0][0])
                else:
                    tokens.append(token)

            tmptokens = tokens
            tokens = []
            for token in tmptokens:
                if "-" in token or "." in token or "_" in token:
                    found = wordpunctuation.findall(token)
                    if found:
                        tokens.extend(found)
                    else:
                        tokens.append(token)
                else:
                    tokens.append(token)

            tokens =  [token for token in tokens if token.isalnum()]
            taggedtokens = nltk.pos_tag(tokens)

            lemmatizer = WordNetLemmatizer()
            lemmas = [lemmatizer.lemmatize(token, tag_dict.get(tag[0], wordnet.NOUN)) for token, tag in taggedtokens]
            lemmas = [lemma.lower() for lemma in lemmas]
            results.append((id, " ".join(lemmas)))

        with open("tedlemmatized.csv", "w") as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(["id", "transcript"])
            for id, row in results:
                writer.writerow([id, row])

