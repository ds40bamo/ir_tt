#!/bin/python

import matplotlib.pyplot as plt
import numpy as np
import json
from os import path

TOPICS_PATH = path.join(path.dirname(path.realpath(__file__)), "topics")
FILE = "evaluation.json"
EVALUATION_PATH = path.join(TOPICS_PATH, FILE)
METRICS = ("precision", "mean reciprocal rank", "ndcg")
COLORS = ("r", "g", "b", "y")

if __name__ == "__main__":
    with open(EVALUATION_PATH, "r") as f:
        evaluation = json.load(f)
        plotinfos = []
        for timestamp, evalinfo in evaluation.items():
            plotinfo = {
                "timestamp": timestamp,
                "k": evalinfo["k"],
                "name": evalinfo["name"],
                "metrics": evalinfo["metrics"],
            }
            plotinfos.append(plotinfo)

    plotinfos.sort(key=lambda x: float(x["timestamp"]))
    width = 0.1
    ind = np.arange(len(plotinfos))
    for i, (metric, color) in enumerate(list(zip(METRICS, COLORS))):
        barind = ind - len(METRICS)/2*width + width/2 + width*i
        plt.bar(barind, [plotinfo["metrics"][metric] for plotinfo in plotinfos], width, color=color, label=metric)
    plt.title("evaluation (@{})".format(plotinfos[0]["k"]))
    plt.ylim([0,1])
    labels = [plotinfo["name"] for plotinfo in plotinfos]
    plt.xticks(ind, labels)
    plt.legend()
    plt.savefig("result.png")
