#!/bin/sh

pipenv install
pipenv run python -c "import nltk; nltk.download('stopwords'); nltk.download('punkt'); nltk.download('wordnet'); nltk.download('averaged_perceptron_tagger')"
pipenv run flask create-index-if-not-exists --force
pipenv run flask run -h 0.0.0.0
